<?php
/**
 * Ulrichsaxofoon Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ulrichsaxofoon
 */

add_action( 'wp_enqueue_scripts', 'storefront_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function storefront_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'storefront-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'ulrichsaxofoon-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'storefront-style' )
	);

}

// remove storefront actions
function remove_sf_actions() {
	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	remove_action( 'storefront_footer', 'storefront_credit',20);

}
add_action( 'init', 'remove_sf_actions' );


function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
			global $template;
			print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );